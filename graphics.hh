#ifndef __GRAPHICS_HH__
#define __GRAPHICS_HH__

#include <limits>
#include <cmath>

namespace graphics{
	template<typename T>
	class Framebuffer{
	private:
		int width, height;
		T* data;
	public:
		Framebuffer(int width, int height);
		virtual ~Framebuffer();

		void clear(T value);
		inline bool in_bounds(int x, int y);	
	};

	struct RGBA{
		unsigned char red;
		unsigned char green;
		unsigned char blue;
		unsigned char alpha;
	};

	class Raster{
	private:
		Framebuffer<RGB>	color;
		Framebuffer<float>	depth;

		unsigned int width;
		unsigned int height;

		bool depth_test;

		Mat4 viewport;
	public:
		enum
		ClearFlags
		{
			CLEAR_COLOR = 0b10000000,
			COLOR_DEPTH = 0b01000000
		};
		
		Raster(u32 width, u32 height): color(width, height), depth(width, height) { }
		virtual ~Raster() { }

		void clear(int flags);
		void line(float x1, float y1, float x2, float y2, RGB color);
	};
}

#endif /* __GRAPHICS_HH__ */
