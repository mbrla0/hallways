########################
### General settings ###
########################
PROJECT		= hallways
CXX			= clang++
CXXFLAGS	= -g -pedantic -Wall -Wextra -std=c++17 -O0 -DMATH_SSE2
LDFLAGS		=

# Library definitions
LDFLAGS		+= 
CXXFLAGS	+= 

########################################
### System-dependant module settings ###
########################################

# input/ module
INPUT_SOURCE	= input/evdev.cc	# Evdev source file
INPUT_METHOD	= 1					# Evdev input method
INPUT_LIBS		= 

# audio/ module
AUDIO_SOURCE	= audio/alsa.cc	# ALSA source file
AUDIO_METHOD	= 1				# ALSA audio system
AUDIO_LIBS		= -lasound		# ALSA library

# window/ modules
WINDOW_SOURCE	= window/xlib.cc	# Xlib interface source
WINDOW_METHOD	= 1					# Xlib windowing method
WINDOW_LIBS		= -lX11				# Xlib

LDFLAGS  += $(INPUT_LIBS) $(AUDIO_LIBS) $(WINDOW_LIBS)
CXXFLAGS += -DAUDIO_METHOD=$(AUDIO_METHOD) \
			-DINPUT_METHOD=$(INPUT_METHOD) \
			-DWINDOW_METHOD=$(WINDOW_METHOD)
MODULE_SOURCES = \
	$(INPUT_SOURCE:.cc=.o) \
	$(AUDIO_SOURCE:.cc=.o) \
	$(WINDOW_SOURCE:.cc=.o)

###################
### Build rules ###
###################
$(PROJECT): $(MODULE_SOURCES) math/math.o main.o
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -o $@ $^

%.o: %.cc %.hh
	$(CXX) $(CXXFLAGS) -c -o $@ $<

clean:
	find . -type f -name "*.o" -delete
	rm -rf $(PROJECT)
