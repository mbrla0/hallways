#define _POSIX_C_SOURCE 200200L
#include <time.h>
#include <fstream>
#include <cstring>
#include <cmath>
#include "input/input.hh"
#include "audio/audio.hh"
#include "window/window.hh"
#include "math/math.hh"

int
main(int argc, char **argv)
{
	math::mat4f matrix0 = {{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}};
	math::mat4f matrix1 = {{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}};
	math::mat4f matrix2 = {{2, 0, 0, 0, 0, 2, 0, 0, 0, 0, 2, 0, 0, 0, 0, 2}};
	math::mat4f result0 = matrix0 * matrix2;
	math::mat4f result1 = matrix0 * matrix1;
	for(int i = 0; i < 16; ++i){
		std::cout << std::to_string(result0.a[i]) << " ";
	}
	std::cout << std::endl;
	for(int i = 0; i < 16; ++i){
		std::cout << std::to_string(result1.a[i]) << " ";
	}
	std::cout << std::endl;


	/* Open evdev devices manually */
	std::string msname = "/dev/input/mouse0";
	std::string kbname = "/dev/input/event15";
	input::evdev::EvdevMouse    ek(msname);
	input::evdev::EvdevKeyboard kb(kbname);

	input::IKeyboard& kboard = kb;
	input::IMouse&    mouse  = ek;

	std::ifstream music("target/sample_audio.s16le_48000", std::ios::binary);
	std::unique_ptr<audio::IVoice> voice =
		audio::open_voice(48000, 2, audio::AUDIO_FORMAT_S16LE);

	std::unique_ptr<window::IWindow> window = 
		window::create_window(800, 600);

	int framecount = 0;
	while(!window->closed()){
		++framecount;
		window->set_title(std::string("AAAAAAAAA frame ") + std::to_string(framecount));

		kboard.saturate();
		mouse.saturate();

		if(music.eof()){
			std::cout << "Seeking to the beggining" << std::endl;
			music.seekg(0, music.beg);
			if(!music){
				exit(1);
			}
		}

		if(!window->is_focused())
			goto ignore_input;

		static bool pressed = false;
		static bool capture = false;
		if(!pressed){
			if(kboard.is_pressed(KEY_C)){
				capture = !capture;
				window->lock_cursor(capture);
				pressed = true;
			}
		}else{
			if(!kboard.is_pressed(KEY_C))
				pressed = false;
		}

ignore_input:
		voice->saturate(music);

//		std::cout << "\rIs A pressed? " << kboard.is_pressed(KEY_A) << std::endl;
//		std::cout << "Is LMB pressed? " << mouse.is_pressed(MOUSE_LEFT) << std::endl; 
//		std::cout << "dx: " << mouse.dx() << ", dy: " << mouse.dy() << std::endl << std::endl;

		voice->feed();

		/* Paint Window */
		static window::Pixel *pixmap_data = new window::Pixel[window->w() * window->h()];
		static window::Pixmap pixmap = {window->w(), window->h(), pixmap_data};
		if(window->reconfigured()){
			delete[] pixmap_data;
			pixmap_data = new window::Pixel[window->w() * window->h()];

			pixmap.width = window->w();
			pixmap.height = window->h();
			pixmap.data = pixmap_data;
		}

		/* Draw */
		std::memset(pixmap_data, 0xff, pixmap.width * pixmap.height * 4);

		static float x = 400;
		static float angle = 0;
		
		static int y = 0;
		for(y = 0; y < 200; ++y)
			std::memset(&pixmap_data[(200 + y) * pixmap.width + (int) std::round(x)], 0x00, 800);
		
		angle += 1.0 / 60.0;
		x = 300.0 + std::sin(angle) * 300.0;

		window->paint(pixmap);
		window->update();

		struct timespec time;
		time.tv_sec = 0;
		time.tv_nsec = 1000000000 / 60;
		nanosleep(&time, NULL);
	}

	return 0;
}
