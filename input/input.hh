#ifndef __INPUT_HH__
#define __INPUT_HH__

/* Method-independent input code. */
#include "prelude.hh"

#define INPUT_METHOD_EVDEV 1

#if INPUT_METHOD == INPUT_METHOD_EVDEV
#include "evdev.hh"
#endif


#endif
