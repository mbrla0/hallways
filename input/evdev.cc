#include "evdev.hh"
using namespace input::evdev;

EvdevMouse::EvdevMouse(std::string& fname)
{
	int fd = open(fname.c_str(), O_RDONLY | O_NONBLOCK);
	if(fd == -1){
		perror("could not open mouse");
		throw errno;
	}

	this->mouse_fd = fd;
	this->_dx = 0;
	this->_dy = 0;
}

EvdevMouse::~EvdevMouse(){
	close(this->mouse_fd);
}

int
EvdevMouse::poll_event()
{
	EvdevMouse::Event event;

	int result = read(this->mouse_fd, &event, sizeof(EvdevMouse::Event));
	if(result == -1){
		switch(errno){
		case EAGAIN:
//			std::cerr << "[EvdevMouse] No events to poll." << std::endl;
			return 0;
		default:
			perror("[EvdevMouse] Cannot read event");
			return -1;
		}
	}else if(result != sizeof(EvdevMouse::Event)){
		std::cerr << "[EvdevMouse] Early read, expected to read " << sizeof(EvdevMouse::Event) << " but read " << result << std::endl;
		return 0;
	}

	/* Accumulate effects until they've been read */
	this->_dx += event.dx;
	this->_dy += event.dy;

	/* Save buttons */
	this->button_map = event.buttons;	

	return 1;
}

int
EvdevMouse::wait_event()
{
	/* Allow reading operations to block */
	int flags = fcntl(this->mouse_fd, F_GETFD);
	fcntl(this->mouse_fd, F_SETFD, flags & ~O_NONBLOCK);

	/* Poll next event, which may now block */
	int result = this->poll_event();

	/* Return to nonblocking mode */
	fcntl(this->mouse_fd, F_SETFD, flags);

	return result;
}

void
EvdevMouse::saturate()
{
	/* Poll until a 0 is returned */
	while(this->poll_event() > 0) { }
}

int
EvdevMouse::dx()
{
	int dx = this->_dx;
	this->_dx = 0;

	return dx;
}

int
EvdevMouse::dy()
{
	int dy = this->_dy;
	this->_dy = 0;

	return dy;
}

bool
EvdevMouse::is_pressed(int button)
{
	return (this->button_map << button) & 1;
}

EvdevKeyboard::EvdevKeyboard(std::string& fname)
{
	int fd = open(fname.c_str(), O_RDONLY | O_NONBLOCK);
	if(fd == -1){
		perror("could not open keyboard");
		throw errno;
	}

	this->keyboard_fd = fd;
	std::memset(this->keymap, 0, KEY_CNT);
}

EvdevKeyboard::~EvdevKeyboard(){
	close(this->keyboard_fd);
}

int
EvdevKeyboard::poll_event(){
	EvdevKeyboard::Event event;
	
	int result = read(this->keyboard_fd, &event, sizeof(EvdevKeyboard::Event));
	if(result == -1){
		switch(errno){
		case EAGAIN:
//			std::cerr << "[EvdevKeyboard] No events to poll." << std::endl;
			return 0;
		default:
			perror("[EvdevKeyboard] Cannot read event");
			return -1;
		}
	}else if(result != sizeof(EvdevKeyboard::Event)){
		std::cerr << "[EvdevKeyboard] Early read." << std::endl;
		return -1;
	}

	if(event.type != EV_KEY){
		/* Non key-related events are discarded for now */	
		return 1;
	}

	/* Event codes for keypresses are:
	 * 		0 -> released
	 * 		1 -> pressed
	 * 		2 -> autorepeat
	 */
	this->keymap[event.code] = event.value;

	return 1;
}

int
EvdevKeyboard::wait_event()
{
	/* Allow reading operations to block */
	int flags = fcntl(this->keyboard_fd, F_GETFD);
	fcntl(this->keyboard_fd, F_SETFD, flags & ~O_NONBLOCK);

	/* Poll next event, which may now block */
	int result = this->poll_event();

	/* Return to nonblocking mode */
	fcntl(this->keyboard_fd, F_SETFD, flags);

	return result;
}

void
EvdevKeyboard::saturate()
{
	/* Poll until a 0 is returned */
	while(this->poll_event() > 0) { }
}

int
EvdevKeyboard::is_pressed(int keycode)
{
	return this->keymap[keycode];
}
