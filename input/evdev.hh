#ifndef __INPUT_EVDEV_HH__
#define __INPUT_EVDEV_HH__

#include <iostream>						/* For logging 				*/
#include <cstring>						/* For memset()				*/
#include <stdio.h>						/* For perror()				*/
#include <unistd.h>						/* For open() and read()	*/
#include <fcntl.h>						/* For fnctl()				*/
#include <errno.h>						/* For errno 				*/
#include <linux/input-event-codes.h>	/* For evdev input codes	*/

#include "prelude.hh"

/* Mouse button codes */
#define MOUSE_LEFT   0
#define MOUSE_RIGHT  1
#define MOUSE_MIDDLE 2

namespace input{
	namespace evdev{
		class EvdevMouse: public IMouse{
		protected:
			int mouse_fd;

			int _dx;
			int _dy;
			char button_map;

			struct Event{
				char buttons;
				char dx;
				char dy;
			};

		public:
			EvdevMouse(std::string& filename);
			virtual ~EvdevMouse();

			virtual void saturate();
			virtual int poll_event();
			virtual int wait_event();

			virtual int dx();
			virtual int dy();
			virtual bool is_pressed(int button);
		};

		class EvdevKeyboard: public IKeyboard{
		protected:
			int keyboard_fd;
			bool keymap[KEY_CNT];
			struct Event{
				struct timeval time;
				unsigned short type;
				unsigned short code;
				unsigned int value;
			};

		public:
			EvdevKeyboard(std::string& filename);
			virtual ~EvdevKeyboard();

			virtual void saturate();
			virtual int poll_event();
			virtual int wait_event();

			virtual int is_pressed(int keycode);
		};
	}/* input::evdev */

}

#endif
