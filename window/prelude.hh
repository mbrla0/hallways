#ifndef __WINDOW_PRELUDE_HH__
#define __WINDOW_PRELUDE_HH__

#include <string>
#include <memory>

namespace window{
	/* RGBA pixel */
	struct Pixel{ unsigned char r, g, b, a; };
	
	/* RGBA Pixmap.
	 *
	 * TODO: Maybe implement support for other pixel
	 * formats in the future.
	 */
	struct Pixmap{
		unsigned int width;
		unsigned int height;
		Pixel* data;
	};

	class IWindow{
	public:
		virtual signed int x() = 0;		/* X coordinate of the window	*/
		virtual signed int y() = 0;		/* Y coordinate of the window	*/
		virtual unsigned int w() = 0;	/* Width of the window			*/
		virtual unsigned int h() = 0;	/* Height of the window			*/
		
		/* Whether or not the window's state (width, height, x and y)
		 * has changed since the last call to this function */
		virtual bool reconfigured() = 0;

		/* Sets the title of the window */
		virtual void set_title(const std::string&) = 0;

		/* Whether or not this window has been closed */
		virtual bool closed() = 0;

		/* Set whether or not to lock the cursor inside the window */
		virtual void lock_cursor(bool) = 0;

		/* Returns whether or not this window can have transparency */
		virtual bool supports_alpha() = 0;
	
		/* Is this window focused? */
		virtual bool is_focused() = 0;

		/* Paints a pixmap onto the window */
		virtual void paint(const Pixmap&) = 0;

		/* Process all pending window events and performs
		 * necessary updates to the window */
		virtual void update() = 0;
	};

	std::unique_ptr<IWindow>
	create_window(unsigned int width, unsigned int height);
}

#endif
