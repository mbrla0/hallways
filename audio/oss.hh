#ifndef __AUDIO_OSS_HH__
#define __AUDIO_OSS_HH__

#include "prelude.hh"
#include <sys/ioctl.h>		/* For ioctl() 					*/
#include <sys/soundcard.h>	/* For everything OSS-related	*/
#include <fcntl.h>			/* For O_WRONLY, O_NONBLOCK		*/
#include <stdio.h>			/* For perror()					*/
#include <unistd.h>			/* For open(), write(), etc.	*/
#include <iostream>			/* For std::cerr, std::cout		*/

namespace audio{
	namespace oss{
		unsigned int format_to_oss(Format f);
		Format oss_to_format(unsigned int f);

		class OssVoice: public IVoice{
		protected:
			int fd; /* Output device file descriptor */
			
			/* A set of two buffers to be used as front
			 * and back buffers for playback, holding 
			 * approx. 1 second of playback in the current
			 * settings. */
			struct{
				unsigned int rpointer;
				unsigned int wpointer;
				unsigned int length;
				char *data;
			} buffers[2];
			void swap_buffers();

			/* Current index of the front buffer. Either 0
			 * or 1, corresponding to the buffers[] index.
			 * The front buffer should not be written to as
			 * the data in it is currently being played back.
			 * All changes to the audio data should be done in
			 * the back buffer (buffers[1 - front]).
			 */ 
			unsigned int front;

			/* Since OSS specifies no simple method for retrieving
			 * output attributes such as sampelrate and channel count,
			 * save the values it returns as a result of setting those
			 * attributes. */
			unsigned int _samplerate;
			unsigned int _channels;
			Format _format;
		public:
			OssVoice(
					std::string location,
					unsigned int samplerate,
					unsigned int channels,
					Format format);
			virtual ~OssVoice();

			virtual unsigned int channels() const;
			virtual unsigned int samplerate() const;
			virtual Format format() const;

			virtual void set_channels(unsigned int);
			virtual void set_samplerate(unsigned int);
			virtual void set_format(Format);

			virtual void saturate(std::istream&);
			virtual void feed();
		};
	}
}

#endif
