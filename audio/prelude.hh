#ifndef __AUDIO_PRELUDE_HH__
#define __AUDIO_PRELUDE_HH__

#include <istream>	/* For std::istream		*/
#include <memory>	/* For std::unique_ptr	*/

#define AUDIO_FORMAT_BIGENDIAN_BIT	0x10	/* Flags a format as big-endian, assumed little-endian otherwise 		*/
#define AUDIO_FORMAT_SIGNED_BIT		0x20	/* Flags a format as signed, assumed unsigned otherwise					*/
#define AUDIO_FORMAT_FLOATING_BIT	0x40	/* Flags a format as being floating point, assumed integer otherwise	*/
#define AUDIO_FORMAT_SIZE_MASK		0x0F	/* Part of the format value containing its size in bytes				*/

/* Returns the size in bytes for a given format */
#define AUDIO_FORMAT_SIZE(format) (format & AUDIO_FORMAT_SIZE_MASK)

namespace audio{
	enum Format{
		AUDIO_FORMAT_S16LE	= 0x22, AUDIO_FORMAT_S16BE	= 0x32,
		AUDIO_FORMAT_U16LE	= 0x02, AUDIO_FORMAT_U16BE	= 0x12,
		AUDIO_FORMAT_F32	= 0x44, AUDIO_FORMAT_S8		= 0x21
	};

	class IVoice{
	public:
		/* Number of samples being output per second */
		virtual unsigned int samplerate() const = 0;

		/* Number of output channels */
		virtual unsigned int channels() const = 0;

		/* Format in which the samples are stored */
		virtual Format format() const = 0;

		/* Functions for setting those values */
		virtual void set_samplerate(unsigned int)	= 0;
		virtual void set_channels(unsigned int)		= 0;
		virtual void set_format(Format)				= 0;
		
		/* Reads as much as needed from the input stream
		 * as needed to completely fill the internal buffer. */
		virtual void saturate(std::istream&) = 0;

		/* Feeds from the internal buffer into the audio system.
		 * Must not block. */
		virtual void feed() = 0;
	};

	/* Tries opening a new voice using the given paramenters.
	 * May throw in the case of an error. Implementation files
	 * add this function to them.
	 */
	std::unique_ptr<IVoice>
	open_voice(unsigned int samplerate, unsigned int channels, Format format);
}

#endif
