#ifndef __AUDIO_ALSA_HH__
#define __AUDIO_ALSA_HH__

#include <alsa/asoundlib.h>	/* For everything ALSA-related */

#include "prelude.hh"

namespace audio{
	namespace alsa{
		/* Converts a Format to an ALSA format integer */
		snd_pcm_format_t format_to_alsa(Format f);

		class AlsaVoice: public IVoice{
		protected:
			std::string device_name;
			Format _format;

			/* ALSA PCM device handle */
			snd_pcm_t* sound_device;

			/* Hardware Paramenters of the PCM device */
			snd_pcm_hw_params_t *hw_params;

			/* Front and back buffers of audio data. */
			struct{
				unsigned int rpointer;
				unsigned int wpointer;
				unsigned int length;
				char *data;
			} buffers[2];
			unsigned char front;

			/* Swaps the back and front buffers */
			void swap_buffers();
		public:
			AlsaVoice(
					std::string device_name,
					unsigned int samplerate,
					unsigned int channels,
					Format format);
			virtual ~AlsaVoice();

			virtual unsigned int samplerate() const;
			virtual unsigned int channels() const;
			virtual Format format() const;

			virtual void set_samplerate(unsigned int);
			virtual void set_channels(unsigned int);
			virtual void set_format(Format);

			virtual void saturate(std::istream&);
			virtual void feed();
		};
	}
}

#endif
