#include "oss.hh"
using namespace audio;

unsigned int
oss::format_to_oss(Format f)
{
	switch(f){
	case AUDIO_FORMAT_S16LE:
		return AFMT_S16_LE;
	case AUDIO_FORMAT_S16BE:
		return AFMT_S16_BE;
	case AUDIO_FORMAT_U16LE:
		return AFMT_U16_LE;
	case AUDIO_FORMAT_U16BE:
		return AFMT_U16_BE;
	case AUDIO_FORMAT_S8:
		return AFMT_S8;
	case AUDIO_FORMAT_F32:
		return AFMT_FLOAT;
	}

	return 0;
}

Format
oss::oss_to_format(unsigned int f)
{
	switch(f){
	case AFMT_S16_LE:
		return AUDIO_FORMAT_S16LE;
	case AFMT_S16_BE:
		return AUDIO_FORMAT_S16BE;
	case AFMT_U16_LE:
		return AUDIO_FORMAT_U16LE;
	case AFMT_U16_BE:
		return AUDIO_FORMAT_U16BE;
	case AFMT_S8:
		return AUDIO_FORMAT_S8;
	case AFMT_FLOAT:
		return AUDIO_FORMAT_F32;
	}

	return AUDIO_FORMAT_S16LE;
}

oss::OssVoice::OssVoice(std::string filename,
		unsigned int samplerate,
		unsigned int channels,
		Format format){

	int fd = open(filename.c_str(), O_WRONLY | O_NONBLOCK);
	if(fd < 0){
		perror("[OssVoice] Could not open audio output device");
		throw errno;
	}
	this->fd = fd;

	/* Set the samplerate and channel count*/
	this->set_channels(channels);
	this->set_samplerate(samplerate);
	this->set_format(format);

	this->buffers[0] = {
		0,
		0,
		samplerate * channels * AUDIO_FORMAT_SIZE(format),
		new char[samplerate * channels * AUDIO_FORMAT_SIZE(format)]
	};
	this->buffers[1] = {
		0,
		0,
		samplerate * channels * AUDIO_FORMAT_SIZE(format),
		new char[samplerate * channels * AUDIO_FORMAT_SIZE(format)]
	};
	this->front = 1;
}

oss::OssVoice::~OssVoice(){
	close(this->fd);

	delete[] this->buffers[0].data;
	delete[] this->buffers[1].data;
}

void
oss::OssVoice::set_samplerate(unsigned int rate)
{	
	unsigned int r = rate; 
	if(ioctl(this->fd, SNDCTL_DSP_SPEED, &r) < 0){
		perror("[OssVoice] Setting samplerate failed");
		return;
	}

	this->_samplerate = r;
}

void
oss::OssVoice::set_channels(unsigned int channels)
{
	unsigned int c = channels;
	if(ioctl(this->fd, SNDCTL_DSP_CHANNELS, &c) < 0){
		perror("[OssVoice] Setting channel count failed");
		return;
	}

	this->_channels = c;
}

void
oss::OssVoice::set_format(Format format)
{
	unsigned int fmt = oss::format_to_oss(format);
	unsigned int tmp = fmt;
	if(ioctl(this->fd, SNDCTL_DSP_SETFMT, &tmp) < 0){
		perror("[OssVoice] Setting format failed");
		return;
	}

	if(tmp != fmt){
		std::cerr << "[OssVoice] Format mismatch" << std::endl;
	}
	this->_format = oss::oss_to_format(tmp);
}

Format
oss::OssVoice::format() const
{ return this->_format; }

unsigned int
oss::OssVoice::channels() const
{ return this->_channels; }

unsigned int
oss::OssVoice::samplerate() const
{ return this->_samplerate; }

void
oss::OssVoice::swap_buffers()
{
	/* Swap the buffers */
	this->front = 1 - this->front;

	/* Reset the current back buffer's pointers */
	auto back = &this->buffers[1 - this->front];
	back->rpointer = 0;
	back->wpointer = 0;	
}

void
oss::OssVoice::saturate(std::istream& source)
{
	auto back = &this->buffers[1 - this->front];
	int pending = back->length - back->wpointer;

	source.read(&back->data[back->wpointer], pending);
	back->wpointer += source.gcount();
}

void
oss::OssVoice::feed()
{
	auto front = &this->buffers[this->front];
	/* Check for the need of a buffer swap */
	if(front->rpointer >= front->wpointer){
		this->swap_buffers();

		auto new_front = &this->buffers[this->front];
		if(new_front->rpointer >= front->wpointer)
			return;
		else
			return this->feed();
	}

	/* Write as much as possible to the output device */
	int pending = front->wpointer - front->rpointer;
	int written = write(this->fd, &front->data[front->rpointer], pending);
	if(written < 0){
		perror("[OssVoice] Could not write to output device");
	}else front->rpointer += written;
}

/* Generic prelude glue */
std::unique_ptr<IVoice>
audio::open_voice(unsigned int samplerate, unsigned int channels, Format fmt)
{
	static unsigned int vp = 0;
	static unsigned int tries = 0;

	audio::oss::OssVoice *voice = nullptr;
	bool success = false;
	for(tries = 0; tries < 10; ++tries){
		std::string fname = "/dev/dsp0.vp" + std::to_string(vp + tries);
		try{
			voice = new audio::oss::OssVoice(fname, samplerate, channels, fmt);
			success = true;
		}catch(int err){
			std::cerr << "[oss] Could not open device " << fname << std::endl;	
		}
	}
	vp += tries;

	if(!success){
		std::cerr << "[oss] Maximum number of tries exceeded." << std::endl;
		throw -1;
	}

	return std::unique_ptr<IVoice>(voice);
}
