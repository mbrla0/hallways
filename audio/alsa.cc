#include "alsa.hh"
using namespace audio;

snd_pcm_format_t
alsa::format_to_alsa(Format format){
	switch(format){
	case AUDIO_FORMAT_S16LE:
		return SND_PCM_FORMAT_S16_LE;
	case AUDIO_FORMAT_S16BE:
		return SND_PCM_FORMAT_S16_BE;
	case AUDIO_FORMAT_U16LE:
		return SND_PCM_FORMAT_U16_LE;
	case AUDIO_FORMAT_U16BE:
		return SND_PCM_FORMAT_U16_BE;
	case AUDIO_FORMAT_F32:
		return SND_PCM_FORMAT_FLOAT;
	case AUDIO_FORMAT_S8:
		return SND_PCM_FORMAT_S8;
	}
}

alsa::AlsaVoice::AlsaVoice(
		std::string device_name,
		unsigned int samplerate,
		unsigned int channels,
		Format format)
{
	this->_format = format;
	this->device_name = device_name;

	printf("[:] ALSA Subsystem.\n");
	/* Setup ALSA device */
	int error = snd_pcm_open(&this->sound_device, device_name.c_str(), SND_PCM_STREAM_PLAYBACK, 0); 
	if(error != 0){
		printf("[-] Could not open ALSA device %s: %s\n", device_name.c_str(), snd_strerror(error));
		throw error;
	}
	printf("[+] Successfully opened ALSA device %s.\n", device_name.c_str());

	/* Set to ALSA handle to nonblocking mode */
	error = snd_pcm_nonblock(this->sound_device, 1);
	if(error < 0){
		printf("[-] Unable to set nonblocking mode: %s\n", snd_strerror(error));
		throw error;
	}

	/* Set format, channel and samplerate */	
	error = snd_pcm_hw_params_malloc(&this->hw_params);
	if(error < 0){
		printf("[-] Unable to allocate memory for hardware parameters: %s\n", snd_strerror(error));
		throw error;
	}

	error = snd_pcm_hw_params_any(this->sound_device, this->hw_params);
	if(error < 0){
		printf("[-] Unable to acquire hardware parameters for device: %s\n", snd_strerror(error));
		throw error;
	}

	error = snd_pcm_hw_params_set_rate_resample(this->sound_device, this->hw_params, 1);
	if(error < 0){
		printf("[-] Unable to set resampling: %s\n", snd_strerror(error));
		throw error;
	}
	printf("[+] Enabled resampling.\n");

	error = snd_pcm_hw_params_set_access(this->sound_device, this->hw_params, SND_PCM_ACCESS_RW_INTERLEAVED);
	if(error < 0){
		printf("[-] Unable to set access mode: %s\n", snd_strerror(error));
		throw error;
	}
	printf("[+] Access mode is RW Interleaved.\n");

	error = snd_pcm_hw_params_set_format(this->sound_device, this->hw_params, format_to_alsa(format));
	if(error < 0){
		printf("[-] Unable to set audio format: %s\n", snd_strerror(error));
		throw error;
	}
	printf("[+] Audio format set to %i.\n", format);

	error = snd_pcm_hw_params_set_channels(this->sound_device, this->hw_params, channels);
	if(error < 0){
		printf("[-] Unable to set channel count to %i: %s\n", channels, snd_strerror(error));
		throw error;
	}
	printf("[+] Channel count set to %i.\n", channels);

	unsigned int set_rate = samplerate;
	signed   int subunit_dir = 0;
	error = snd_pcm_hw_params_set_rate_near(this->sound_device, this->hw_params, &set_rate, &subunit_dir);
	if(error < 0){
		printf("[-] Unable to set samplerate to %i: %s\n", samplerate, snd_strerror(error));
		throw error;
	} else if(set_rate != samplerate){
		printf("[-] Sample rate mismatch! Expected %iHz, got %iHz.\n", samplerate, set_rate);
		throw error;
	}
	printf("[+] Samplerate set to %iHz.\n", set_rate);

	/* Apply parameters */
	error = snd_pcm_hw_params(this->sound_device, this->hw_params);
	if(error < 0){
		printf("[-] Unable to set hardware parameters: %s\n", snd_strerror(error));
		throw error;
	}

	/* Create both front and back audio buffers containing one second of audio */
	this->buffers[0] = {
		0,
		0,
		samplerate * channels * AUDIO_FORMAT_SIZE(format),
		new char[samplerate * channels * AUDIO_FORMAT_SIZE(format)]
	};
	this->buffers[1] = {
		0,
		0,
		samplerate * channels * AUDIO_FORMAT_SIZE(format),
		new char[samplerate * channels * AUDIO_FORMAT_SIZE(format)]
	};
	this->front = 1;
}

alsa::AlsaVoice::~AlsaVoice()
{
	snd_pcm_close(this->sound_device);
	snd_pcm_hw_params_free(this->hw_params);

	delete this->buffers[0].data;
	delete this->buffers[1].data;
}

unsigned int
alsa::AlsaVoice::samplerate() const
{
	unsigned int rate;
	/* Since this is only called after the constructor has finished, 
	 * this->hw_params should always contain a value for the samplerate. */
	assert(snd_pcm_hw_params_get_rate(this->hw_params, &rate, nullptr) >= 0);

	return rate;
}

unsigned int
alsa::AlsaVoice::channels() const
{
	unsigned int channels;
	/* Since this is only called after the constructor has finished, 
	 * this->hw_params should always contain a value for the channel count. */
	assert(snd_pcm_hw_params_get_channels(this->hw_params, &channels) >= 0);

	return channels;
}

Format
alsa::AlsaVoice::format() const
{
	return this->_format;
}

void
alsa::AlsaVoice::set_samplerate(unsigned int)
{ /* TODO: Stub */ }

void
alsa::AlsaVoice::set_channels(unsigned int)
{ /* TODO: Stub */ }

void
alsa::AlsaVoice::set_format(Format)
{ /* TODO: Stub */ }

void
alsa::AlsaVoice::swap_buffers()
{
	/* Swap the buffers */
	this->front = 1 - this->front;

	/* Reset the current back buffer's pointers */
	auto back = &this->buffers[1 - this->front];
	back->rpointer = 0;
	back->wpointer = 0;	
}

void
alsa::AlsaVoice::saturate(std::istream& source)
{
	auto back = &this->buffers[1 - this->front];
	int pending = back->length - back->wpointer;

	source.read(&back->data[back->wpointer], pending);
	back->wpointer += source.gcount();
}

void
alsa::AlsaVoice::feed()
{
	/* TODO: AlsaVoice::feed() IS SLOW!
	 * Things to hopefully fix later:
	 *		- Cache voice properties used in 
	 *		  determining how many ALSA audio frames
	 *		  to write.
	 *		- Better way to check for underruns?
	 */
	auto front = &this->buffers[this->front];
	/* Check for the need of a buffer swap */
	if(front->rpointer >= front->wpointer){
		this->swap_buffers();

		auto new_front = &this->buffers[this->front];
		if(new_front->rpointer >= front->wpointer)
			return;
		else
			return this->feed();
	}

	int pending = front->wpointer - front->rpointer;
	int count = snd_pcm_writei(
			this->sound_device,
			&front->data[front->rpointer],
			pending / this->channels() / 2);
	
	if(count < 0)
		switch(-count){
		case EPIPE:
			printf("[-] ALSA buffer underrun.\n");
			snd_pcm_prepare(this->sound_device);
			break;
		case EAGAIN:
			break;
		default:	
			printf("[-] ALSA writei error: %s\n", snd_strerror(count));
		}
	else
		front->rpointer += count * this->channels() * 2;
}

/* Implementation for the generic audio interface glue
 * function for opening a new voice, tries opening a it
 * in the "default" device. */
std::unique_ptr<IVoice>
audio::open_voice(unsigned int samplerate, unsigned int channels, Format format)
{
	return std::unique_ptr<IVoice>(
		new alsa::AlsaVoice("default", samplerate, channels, format)
	);
}
