#ifndef __BENCHMARK_HH__
#define __BENCHMARK_HH__

#include <functional>
#include <chrono>
#include <string>
#include <iostream>

namespace bench{
	void run(std::function<void(void)>& f, std::string& name){
		namespace c = std::chrono;
		typedef c::high_resolution_clock clock;
		typedef clock::time_point timep;

		std::cerr << "running benchmark " << name << "... ";
		unsigned long long t = 100;
		for(int i = 0; i < 10000; ++i){
			timep beg = clock::now();
			f();
			timep end = clock::now();
			
			if(t != 0)
				t = (t + c::duration_cast<c::nanoseconds>(end - beg).count()) / 2;
			else
				t = c::duration_cast<c::nanoseconds>(end - beg).count();
		}

		std::cerr << std::to_string(t) << " us/op" << std::endl;
	}
}

#endif
