#include "math.hh"
using namespace math;

/* Implement trivial Matrix <-> Matrix operations
 * using a macro file. See mat4_mat4.macro on more
 * details on the definitions that affect the generic
 * implementation.
 *
 * This is a quick and dirty solution I came up with
 * to avoid code redundancy until I possibly find a
 * way to implement this better using a single templated
 * generic matrix structure with SIMD capabilities. */
#define _STRUCT		mat4i
#define _SIMD_TYPE	__m128i
#define _OPERATOR	+
#define _SIMD_FN	_mm_add_epi32
#define _OPERATION	+
#include "mat4_mat4.macro"

#define _OPERATOR	-
#define _SIMD_FN	_mm_sub_epi32
#define _OPERATION	-
#include "mat4_mat4.macro"

#define _STRUCT		mat4f
#define _SIMD_TYPE	__m128
#define _OPERATOR	+
#define _SIMD_FN	_mm_mul_ps
#define _OPERATION	+
#include "mat4_mat4.macro"

#define _OPERATOR	-
#define _SIMD_FN	_mm_sub_ps
#define _OPERATION	-
#include "mat4_mat4.macro"


/* Nontrivial multiplication for both matrix types */
mat4i
mat4i::operator *(const mat4i& b) const
{
	mat4i target;
	/* TODO: SIMD version of the mat4i Matrix <-> Matrix multiplication */
	/* Single operation */
	#define OP(i,j) target.a[j * 4 + i] = \
		  this->a[i]      * b.a[j * 4] \
		+ this->a[i + 4]  * b.a[j * 4 + 1] \
		+ this->a[i + 8]  * b.a[j * 4 + 2] \
		+ this->a[i + 12] * b.a[j * 4 + 3];

	OP(0, 0); OP(0, 1); OP(0, 2); OP(0, 3);
	OP(1, 0); OP(1, 1); OP(1, 2); OP(1, 3);
	OP(2, 0); OP(2, 1); OP(2, 2); OP(2, 3);
	OP(3, 0); OP(3, 1); OP(3, 2); OP(3, 3);

	#undef OP
	return target;
}

mat4f
mat4f::operator *(const mat4f& b) const
{
	mat4f target;
#ifdef MATH_SSE2
	union num{
		__m128 f128;
		float f32[4];
	};

	num *columns[4];
	columns[0] = (num*) &b.a[0];
	columns[1] = (num*) &b.a[4];
	columns[2] = (num*) &b.a[8];
	columns[3] = (num*) &b.a[12];

	#define ROW(id, i_0, i_1, i_2, i_3) \
		rows[id].f32[0] = this->a[i_0]; \
		rows[id].f32[1] = this->a[i_1]; \
		rows[id].f32[2] = this->a[i_2]; \
		rows[id].f32[3] = this->a[i_3]; \

	num rows[4];
	ROW(0, 0, 4, 8,  12);
	ROW(1, 1, 5, 9,  13);
	ROW(2, 2, 6, 10, 14);
	ROW(3, 3, 7, 11, 15);
	#undef NUM

	num sums[4][4];
	sums[0][0].f128 = _mm_mul_ps(columns[0]->f128, rows[0].f128);
	sums[0][1].f128 = _mm_mul_ps(columns[0]->f128, rows[1].f128);
	sums[0][2].f128 = _mm_mul_ps(columns[0]->f128, rows[2].f128);
	sums[0][3].f128 = _mm_mul_ps(columns[0]->f128, rows[3].f128);
	sums[1][0].f128 = _mm_mul_ps(columns[1]->f128, rows[0].f128);
	sums[1][1].f128 = _mm_mul_ps(columns[1]->f128, rows[1].f128);
	sums[1][2].f128 = _mm_mul_ps(columns[1]->f128, rows[2].f128);
	sums[1][3].f128 = _mm_mul_ps(columns[1]->f128, rows[3].f128);
	sums[2][0].f128 = _mm_mul_ps(columns[2]->f128, rows[0].f128);
	sums[2][1].f128 = _mm_mul_ps(columns[2]->f128, rows[1].f128);
	sums[2][2].f128 = _mm_mul_ps(columns[2]->f128, rows[2].f128);
	sums[2][3].f128 = _mm_mul_ps(columns[2]->f128, rows[3].f128);
	sums[3][0].f128 = _mm_mul_ps(columns[3]->f128, rows[0].f128);
	sums[3][1].f128 = _mm_mul_ps(columns[3]->f128, rows[1].f128);
	sums[3][2].f128 = _mm_mul_ps(columns[3]->f128, rows[2].f128);
	sums[3][3].f128 = _mm_mul_ps(columns[3]->f128, rows[3].f128);

	target.a[0]  = sums[0][0].f32[0] + sums[0][0].f32[1] + sums[0][0].f32[2] + sums[0][0].f32[3];
	target.a[1]  = sums[1][0].f32[0] + sums[1][0].f32[1] + sums[1][0].f32[2] + sums[1][0].f32[3];
	target.a[2]  = sums[2][0].f32[0] + sums[2][0].f32[1] + sums[2][0].f32[2] + sums[2][0].f32[3];
	target.a[3]  = sums[3][0].f32[0] + sums[3][0].f32[1] + sums[3][0].f32[2] + sums[3][0].f32[3];
	target.a[4]  = sums[0][1].f32[0] + sums[0][1].f32[1] + sums[0][1].f32[2] + sums[0][1].f32[3];
	target.a[5]  = sums[1][1].f32[0] + sums[1][1].f32[1] + sums[1][1].f32[2] + sums[1][1].f32[3];
	target.a[6]  = sums[2][1].f32[0] + sums[2][1].f32[1] + sums[2][1].f32[2] + sums[2][1].f32[3];
	target.a[7]  = sums[3][1].f32[0] + sums[3][1].f32[1] + sums[3][1].f32[2] + sums[3][1].f32[3];
	target.a[8]  = sums[0][2].f32[0] + sums[0][2].f32[1] + sums[0][2].f32[2] + sums[0][2].f32[3];
	target.a[9]  = sums[1][2].f32[0] + sums[1][2].f32[1] + sums[1][2].f32[2] + sums[1][2].f32[3];
	target.a[10] = sums[2][2].f32[0] + sums[2][2].f32[1] + sums[2][2].f32[2] + sums[2][2].f32[3];
	target.a[11] = sums[3][2].f32[0] + sums[3][2].f32[1] + sums[3][2].f32[2] + sums[3][2].f32[3];
	target.a[12] = sums[0][3].f32[0] + sums[0][3].f32[1] + sums[0][3].f32[2] + sums[0][3].f32[3];
	target.a[13] = sums[1][3].f32[0] + sums[1][3].f32[1] + sums[1][3].f32[2] + sums[1][3].f32[3];
	target.a[14] = sums[2][3].f32[0] + sums[2][3].f32[1] + sums[2][3].f32[2] + sums[2][3].f32[3];
	target.a[15] = sums[3][3].f32[0] + sums[3][3].f32[1] + sums[3][3].f32[2] + sums[3][3].f32[3];
#else
	
	/* Single operation */
	#define OP(i,j) target.a[j * 4 + i] = \
		  this->a[i]      * b.a[j * 4] \
		+ this->a[i + 4]  * b.a[j * 4 + 1] \
		+ this->a[i + 8]  * b.a[j * 4 + 2] \
		+ this->a[i + 12] * b.a[j * 4 + 3];

	OP(0, 0); OP(0, 1); OP(0, 2); OP(0, 3);
	OP(1, 0); OP(1, 1); OP(1, 2); OP(1, 3);
	OP(2, 0); OP(2, 1); OP(2, 2); OP(2, 3);
	OP(3, 0); OP(3, 1); OP(3, 2); OP(3, 3);

	#undef OP

#endif
	return target;
}
#define MAT4_SCALAR_OP(base, type, op) \
	base \
	base::operator op(const type& scalar) const \
	{ \
		base target; \
		\
		target.a[0]  = this->a[0]  op scalar; \
		target.a[1]  = this->a[1]  op scalar; \
		target.a[2]  = this->a[2]  op scalar; \
		target.a[3]  = this->a[3]  op scalar; \
		target.a[4]  = this->a[4]  op scalar; \
		target.a[5]  = this->a[5]  op scalar; \
		target.a[6]  = this->a[6]  op scalar; \
		target.a[7]  = this->a[7]  op scalar; \
		target.a[8]  = this->a[8]  op scalar; \
		target.a[9]  = this->a[9]  op scalar; \
		target.a[10] = this->a[10] op scalar; \
		target.a[11] = this->a[11] op scalar; \
		target.a[12] = this->a[12] op scalar; \
		target.a[13] = this->a[13] op scalar; \
		target.a[14] = this->a[14] op scalar; \
		target.a[15] = this->a[15] op scalar; \
		\
		return target; \
	}

MAT4_SCALAR_OP(mat4i, int, +);
MAT4_SCALAR_OP(mat4i, int, -);
MAT4_SCALAR_OP(mat4i, int, *);
MAT4_SCALAR_OP(mat4i, int, /);

MAT4_SCALAR_OP(mat4f, float, +);
MAT4_SCALAR_OP(mat4f, float, -);
MAT4_SCALAR_OP(mat4f, float, *);
MAT4_SCALAR_OP(mat4f, float, /);
