#ifndef __MATH_HH__
#define __MATH_HH__

#ifdef MATH_SSE2
/* Include SSE2 headers for SIMD operation*/
#include <emmintrin.h>
#endif

namespace math{
	struct alignas(16) mat4i{
		int a[16];

		/* Matrix <-> Matrix operations */
		mat4i operator +(const mat4i&) const;
		mat4i operator -(const mat4i&) const;
		mat4i operator *(const mat4i&) const;

		/* Matrix <-> Scalar operations */
		mat4i operator +(const int&) const;
		mat4i operator -(const int&) const;
		mat4i operator *(const int&) const;
		mat4i operator /(const int&) const;
	};

	struct alignas(16) mat4f{
		float a[16];

		/* Matrix <-> Matrix operations */
		mat4f operator +(const mat4f&) const;
		mat4f operator -(const mat4f&) const;
		mat4f operator *(const mat4f&) const;

		/* Matrix <-> Scalar operations */
		mat4f operator +(const float&) const;
		mat4f operator -(const float&) const;
		mat4f operator *(const float&) const;
		mat4f operator /(const float&) const;
	};
}

#endif
